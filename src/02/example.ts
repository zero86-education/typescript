// boolean
let isBoolean: boolean;
isBoolean = true;
// isBoolean = 'z'; // TS2322: Type 'string' is not assignable to type 'boolean'.

let isDone: boolean = false;


// number
let num: number;
let integer: number = 6;
let float: number = 3.14;

let hex: number = 0xf00d; // 61453
let binary: number = 0b1010; // 10
let octal: number = 0o744; // 484

let infinity: number = Infinity;
let nan: number = NaN;

function plus(num1: number, num2: number): number {
    return num1 + num2;
}

// string
let red: string = 'Red';
let green: string = 'Green';
let yourColor: string = 'Your color is' + green;
let myColor: string = `My color is ${red}.`;

function strings(str1: string, str2: string): string {
    return str1 + str2;
}


// object(여러 타입의 상위 타입)
let obj: object = {};
let arr: object = [];
let func: object = function () {
};
// let nullValue: object = null; // TS2322: Type 'null' is not assignable to type 'object'.
let date: object = new Date();

const player: object = {name: 'nico'};
// object 라면서 왜 에러내요?
// player.name; // Error
// object 임을 알 뿐이지, 해당 객체에 어떤 프로퍼티와 어떤 타입이 있는지는 모름

const player2: { name: string; age: number } = {name: 'NAME', age: 29};
player2.name = '개선';

// array
let fruits: string[] = ['Apple', 'Banana', 'Mango'];
let fruits2: Array<string> = ['Apple', 'Banana', 'Mango'];
let nums: number[] = [100, 101, 102]; // 오직 숫자 아이템만 허용
let strs: string[] = ['apple', 'banana', 'melon']; // 오직 문자 아이템만 허용
let boos: boolean[] = [true, false, true]; // 오직 불리언 아이템만 허용
let items: { name: string }[] = [{name: '1'}, {name: '2'}]; // 객체를 요소로 가진 배열


// tuple
let tuple: [string, number];
tuple = ['a', 1];
tuple = ['b', 2];

tuple.push(3);
console.log(tuple); // ['b', 2, 3];
tuple.push('777');
console.log(tuple); // ['b', 2, 3, '777'];

// tuple.push(true); // Error - 그렇다고 해서 튜플에 정의되지 않는 타입을 넣을수는 없다.
// push() 함수를 사용하여 요소를 추가하는 경우 처음 할당된 값의 타입과 정확히 일치해야 한다.


// enum
enum Color {
    Red,
    Green,
    Blue,
} // enum 타입

let c: Color; // enum 타입 변수 선언
c = Color.Green; // enum 타입 변수에 enum 값 할당
// c = 'Hello'; // Error - enum 타입 변수에는 반드시 설정된 enum 값 (Red, Green, Blue)만 올수 있음

let d: Color.Red; // enum 값을 타입 자체로도 사용할 수가 있음


// any
const a: any[] = [1, 2, 3, 4];
let b: any = true;
a.push('zz');
b = 10;


// void
function getName(name:string): void {
    // void 타입은 반환값이 없어야 한다.
    // return name; // TS2322: Type 'string' is not assignable to type 'void'.
}
function printName(name:string): void {
    console.log(name);
}


// null, undefined
let nullable: null = null;
let undefinedable: undefined = undefined;

/* strick 모드가 아닐경우에만 대입이 가능함 */
let num: number = undefined;
let str: string = null;
let obj: { a: 1, b: false } = undefined;
let arr: any[] = null;
let und: undefined = null;
let nul: null = undefined;
let voi: void = null;


// never
function invalid(message:string): never {
    throw new Error(message);
}


// unknown
let valueNum: unknown = 10;
let valueStr: unknown = "unknown";
// valueStr.length; // TS18046: 'valueStr' is of type 'unknown'.

if (typeof valueNum === "number") {
    // console.log(valueNum.length); // TS2339: Property 'length' does not exist on type 'number'
}

if (typeof valueStr === "string") {
    console.log(valueStr.length);
}