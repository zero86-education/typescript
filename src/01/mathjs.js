"use strict";
// math.ts: 정적
function sum(a, b) {
    return a + b;
}
sum('10', '20'); // Error: '10'은 number 에 할당될 수 없습니다.
sum(10, 20); // 30
