// 교차 타입

type Artwork = {
    genre: string;
    name: string;
}

type Writing = {
    pages: number;
    name: string;
}

type WriteAndArt = Artwork & Writing // 교차 타입
// 아래와 같다.
// {
//     pages: number;
//     genre: string;
//     name: string;
// }

const writeAndArtObj: WriteAndArt = {
    name: 'hi',
    genre: '모험',
    pages: 300
};


// 교차되는 타입이 동일하지 않다면, 에러가 발생합니다.
type A = {
    name: string;
}
type B = {
    name: number;
}
type AB = A & B;
const abObj: AB = {
    // name: 'zzz' // TS2322: Type 'string' is not assignable to type 'never'.
};


// 유니온 타입
type Bird = {
    flyingSpeed: number;
}
type Horse = {
    runningSpeed: number;
}
type Animal = Bird | Horse; // 둘중에 하나.

function moveAnimal(animal: Animal) {
    // console.log(animal.???) // 뭘 호출해야할까?
    if ('flyingSpeed' in animal) {
        console.log(animal.flyingSpeed);
    }
    if ('runningSpeed' in animal) {
        console.log(animal.runningSpeed);
    }
}

// 아래와 같은 경우, 타입 가드를 통해서 추론이 되게끔 해줘야함
let msg!: string | number;
msg.toString(); // ok
// msg.toFixed(); // error
if (typeof msg === 'string') {
    msg.length; // ok
}
if (typeof msg === 'number') {
    msg.toFixed(); // ok
}