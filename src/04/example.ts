// interface

interface Book {
    name?: string; // 선택적 속성
    price: number;
}

const book1: Book = {
    price: 3000,
};
const book2: Book = {
    name: '럭키짱',
    price: 7777,
};


// 읽기 전용 속성
interface Page {
    readonly text: string;
}
const page:Page = {
    text: 'text'
};
// page.text = 'zzzz'; // Attempt to assign to const or readonly variable

// 함수와 메서드
interface Print {
    getPdf(): void;
    getXls(): void;
    getHwp(fileName: string): void;
}
const printObj: Print = {
    getPdf() {
        console.log('getPdf..');
    },
    getXls() {
        console.log('getXls..');
    },
    getHwp(targetFileName: string) {
        // 호출 시그니처에 이름이 꼭 같을 필요는 없다.
    }
}

// 인터페이스 확장
interface Person {
    name?: string;
}
interface Drinker {
    drink?: string;
}
interface Developer extends Person, Drinker {
    skill?: string;
}
const feDev: Developer = {};
feDev.name = 'josh';
feDev.skill = 'TypeScript';
feDev.drink = 'Beer';


// 속성 재정의
interface AMember {
    name: string | null;
    age: number;
}
interface Member extends AMember {
    name: string; // 최상위 타입에서 일부만 필요한 경우
}


// 인터페이스 병합

interface Merged {
    name: string;
}
interface Merged {
    age: number;
}
// 아래와 같음
// interface Merged {
//     name: string;
//     age: number;
// }

interface MergedProp {
    same(input: string): void;
    phone: string;
    diff(input: string): void;
}
interface MergedProp {
    same(input: string): void;
    // TS2717: Subsequent property declarations must have the same type. Property 'phone' must be of type 'string', but here has type 'number'.
    // phone: number; // error
    diff(input: number): void; // OK
}

const mergerPropObj:MergedProp = {
    same(input: string) {
        console.log(input)
    },
    phone: '01012345679',
    diff(input: string | number) {
    }
}


// 제네릭

// function identity(input) {
//     return input;
// }
//
// identity('abc');
// identity(1234);
// identity({msg: 'msg!'});

// 제네릭으로 개선
function identity<T>(input: T): T {
    return input;
}

identity<string>('abc');
identity<number>(1234);
identity<{msg: string}>({msg: 'msg!'});

const msgPrint = <T>(msg: T) => {console.log(msg)};

// 제네릭 인터페이스
interface Box<T> {
    inside: T;
}

const stringBox: Box<string> = {
    inside: 'abc',
};
const numberBox: Box<number> = {
    inside: 123,
}


// 제네릭과 유니온 타입 차이

class DataStorage<T extends string | number | boolean> {
    private data: T[] = [];

    addItem(item: T) {
        this.data.push(item);
    }

    removeItem(item: T) {
        if (this.data.indexOf(item) === -1) {
            return;
        }
        this.data.splice(this.data.indexOf(item), 1); // -1
    }

    getItems() {
        return [...this.data];
    }
}

const textStorage = new DataStorage<string>();
textStorage.addItem('Max');
textStorage.addItem('Manu');
textStorage.removeItem('Max');
console.log(textStorage.getItems());

class DataStorage {
    private data: (string | number | boolean)[] = [];

    addItem(item: string | number | boolean) {
        this.data.push(item);
    }

    removeItem(item: string | number | boolean) {
        if (this.data.indexOf(item) === -1) {
            return;
        }
        this.data.splice(this.data.indexOf(item), 1); // -1
    }

    getItems() {
        return [...this.data];
    }
}

const textStorage = new DataStorage();
textStorage.addItem('Max');
textStorage.addItem('Manu');
textStorage.removeItem('Max');
console.log(textStorage.getItems());