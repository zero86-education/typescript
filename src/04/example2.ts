// 타입 단언(type assertion)

const rawData = '[\'grace\', \'item\', \'darkSoul3\']';
const any = JSON.parse(rawData); // any
const result = JSON.parse(rawData) as string[]; // string[]

// non-null assertion

const el = document.getElementById('el-id')!;
const mayBeDate = Math.random() > 0.5 ? undefined : new Date();
mayBeDate!; // Date 타입으로 추론

let aaaVar: undefined | { name: string };
// aaaVar = {
//     name: 'zzz'
// };
aaaVar.name; // TS18048: 'aaaVar' is possibly 'undefined'
aaaVar!.name; // OK